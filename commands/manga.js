var request = require('request');
var S = require('string');
var parser = require('xml2json');

//return a list of manga's
function searchManga(user, channelID, parameters, userID)
{   
    var kissanimedata = config.KissAnime
    var url = kissanimedata.url.replace('{type}', 'manga')

    var searchString = parameters.join("+").toLowerCase()

    var searchUrl = url + searchString

    var requestData = {
                    url: searchUrl, 
                    headers: {
                        "Authorization" : "Basic " + new Buffer(kissanimedata.username + ":" + kissanimedata.password).toString("base64")
                    }
                }
    
    request(requestData, function (error, response, body) {
        if (!error && response.statusCode == 200)
        {
            var mangaData = parser.toJson(body, {object:true})
            if(mangaData.manga.entry.length > 0)
            {
                results = 'First ' + kissanimedata.max_entries + ' result(s) for "' + searchString.split('+').join(' ') + '": \n\n'
                mangaData.manga.entry.slice(0, kissanimedata.max_entries).forEach(function(entry){
                    results += "Title: " + entry.title + "\n"
                    results += "Rating: " + entry.score + "/10\n"
                    results += "Chapters: " + entry.chapters + "\n"
                    results += "Volumes: " + entry.volumes + "\n"
                    results += "Status: " + entry.status + "\n"
                    results += "Period: " + entry.start_date + " - " + entry.end_date + "\n"
                    results += "Synopis:\n" + S(entry.synopsis).truncate(250).s + "\n"
                    results += "Url: http://myanimelist.net/manga/" + entry.id + "\n\n"
                })
                m.sendMessage(channelID, results)
            }
            else
            {
                m.sendMessage(channelID, 'No result(s) for "' + searchString.split('+').join(' ') + '".')
            }
        }
        else
        {
            m.sendMessage(channelID, 'No result(s) for "' + searchString.split('+').join(' ') + '".')
            log.error('Failed to run manga data for: ' + searchUrl + '\n' + error)
        }
    })
}

module.exports = {
    command: searchManga
}