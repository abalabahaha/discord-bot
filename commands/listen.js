var models = require('../libs/models');

var silent_channel = models.silent_channel;

function listen(user, channelID, parameters, userID)
{
    silent_channel.findOne({
        where: {
            channel_id: channelID
        }
    }).then(function(silented){
        if(!silented)
        {
            return m.sendMessage(channelID, 'I am still listening to this channel.');
        }

        silented.destroy().then(function(){
            return m.sendMessage(channelID, 'Listening to this channel again!');
        });
    });
}

module.exports = {
    command: listen
}