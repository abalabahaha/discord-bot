//reboot the bot
function reboot(user, channelID, parameters, userID)
{
    if(parameters.length >= 0 && parameters[0] == 'true')
    {
        Object.keys(bot.servers).forEach(function(server){
            m.sendMessage(server, "Going for a reboot! Be right back!")
        })    
    }
    
    bot.disconnect()
}

module.exports = {
    command: reboot
}