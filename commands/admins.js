var getServerAdmins = require('../libs/roles').getServerAdmins;

//show list of admins
function admins(user, channelID, parameters, userID)
{
    if(m.isPrivate(channelID))
    {
        return m.sendMessage(channelID, 'In this private conversation you are my only admin :blush:');
    }

    returnMessage = "List of admins: \n"
    serverID = bot.serverFromChannel(channelID)
    getServerAdmins(serverID).forEach(function(user){
        returnMessage += "- " + bot.servers[serverID].members[user].user.username + "\n"
    });

    m.sendMessage(channelID, returnMessage);
}

module.exports = {
    command: admins
}