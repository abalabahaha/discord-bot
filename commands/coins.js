var getUser = require('./user').getUser;

function showCoins(user, channelID, parameters, userID)
{
    user_id = userID;
    if(parameters.length > 0)
    {
        user_id = parameters[0].replace(/<@/g, '').replace(/>/g, '')
    }

    getUser(user_id, function(user){
        if(user && user_id == userID)
        {
            m.sendMessage(channelID, 'You have ' + user.coins + ' coins.');
        }
        else
        {
            m.sendMessage(channelID, '<@' + user_id + '> has ' + user.coins + ' coins.');
        }
    })
}

function giveCoins(user, channelID, parameters, userID)
{
    if(isNaN(parameters[1]))
    {
        return m.sendMessage(channelID, 'You must define the amount of coins to send');
    }

    amount = parseInt(parameters[1]);

    if(amount <= 0)
    {
        return m.sendMessage(channelID, "You must send atleast 1 coin.");
    }


    receiver = parameters[0].replace(/<@/g, '').replace(/>/g, '')

    getUser(userID, function(user){

        if((user.coins-amount) < 0)
        {
            return m.sendMessage(channelID, "You don't have " + amount + " coins.");
        }

        user.coins = user.coins-amount;

        getUser(receiver, function(receiver){
            if(!receiver)
            {
                return m.sendMessage(channelID, parameters[0] + " was not found in the database yet, maybe he needs to say something?")
            }

            user.save().then(function(){
                receiver.coins = receiver.coins + amount;
                receiver.save().then(function(){
                    m.sendMessage(channelID, "You have successfully send " + parameters[0] + " " + amount + " coins.");
                });
            });
            
        });

    })
}


module.exports = {
    showCoins: showCoins,
    giveCoins: giveCoins
}