var request = require('request');
var S = require('string');
var async = require('async');
var date = new Date();

function rito(user, channelID, parameters, userID)
{
    var region = parameters[0].toLowerCase();

    var regions = ['euw', 'na', 'br', 'eune', 'kr', 'lan', 'oce', 'ru', 'tr'];

    if(regions.indexOf(region) == -1)
    {
        m.sendMessage(channelID, 'No such region is existing.');
        return false;
    }

    if(parameters[1].toLowerCase() == 'summoner')
    {
        return ritoSummoner(user, channelID, parameters, userID);
    }
}

function ritoSummoner(user, channelID, parameters, userID)
{
    var ritoConfig = config.Rito;
    var summoner_name = parameters[2];
    var summonerUrl = ritoConfig.url.replace(/{region}/g, parameters[0]).replace(/{function}/g, 'summoner/by-name/'+ summoner_name).replace(/{version}/g, 'v1.4');

    request(summonerUrl, function (error, response, body) {
        if(response.statusCode == 404)
        {
            m.sendMessage(channelID, 'Summoner "' + summoner_name + '" not found on region ' + parameters[0])
            return false;
        }

        var summonerData = JSON.parse(body);
        summonerData = summonerData[summoner_name.replace(/ /g, '').toLowerCase()];

        var statsUrl = ritoConfig.url.replace(/{region}/g, parameters[0])
                        .replace(/{function}/g, 'stats/by-summoner/' + summonerData.id + '/summary')
                        .replace(/{version}/g, 'v1.3');

        statsUrl += '&season=SEASON' + date.getFullYear();

        request(statsUrl, function (error, response, body) {
            summonerData.stats = JSON.parse(body);

            var entryUrl = ritoConfig.url.replace(/{region}/g, parameters[0]).replace(/{function}/g, 'league/by-summoner/' + summonerData.id + '/entry').replace(/{version}/g, 'v2.5');
            request(entryUrl, function (error, response, body) {
                summonerData.entry = JSON.parse(body);
                

                async.each(summonerData.stats.playerStatSummaries, function(stat){
                    if(stat.playerStatSummaryType == 'RankedSolo5x5')
                    {
                        summonerData.summary = stat;
                        return false;
                    }
                });

                async.each(summonerData.entry[summonerData.id], function(entry){
                    if(entry.queue == 'RANKED_SOLO_5x5')
                    {
                        summonerData.ranked = entry;
                        return false;
                    }
                });


                var championsUrl = ritoConfig.url.replace(/{region}/g, parameters[0])
                        .replace(/{function}/g, 'stats/by-summoner/' + summonerData.id + '/ranked')
                        .replace(/{version}/g, 'v1.3');
                championsUrl += '&season=SEASON' + date.getFullYear();

                request(championsUrl, function (error, response, body) {
                    summonerData.champions = JSON.parse(body).champions;
                    summonerData.champions.sort(sortChampions("-totalSessionsPlayed"));
                    summonerData.champions = summonerData.champions.slice(1, 4);

                    async.eachLimit(summonerData.champions, 1, function(champion, nextChampion){
                        var championStaticDataUrl = ritoConfig.url.replace('{region}', 'global').replace('{region}', 'static-data/na').replace(/{function}/g, 'champion/' + champion.id).replace(/{version}/g, 'v1.2') + '&champData=info'
                        request(championStaticDataUrl, function (error, response, body) {
                            champion.info = JSON.parse(body);
                            nextChampion();
                        });

                    }, function(){

                        var returnData = 'Summoner data for: "' + summoner_name + '": \n\n'

                        returnData += 'Name: ' + summonerData.name + '\n'
                        returnData += 'Rank: ' + S(summonerData.ranked.tier).capitalize().s + ' ' + summonerData.ranked.entries[0].division + '\n\n'

                        returnData += '**Ranked data:** \n'
                        returnData += 'Ranked wins: ' + summonerData.summary.wins + '\n'
                        returnData += 'LP: ' + summonerData.ranked.entries[0].leaguePoints + '\n'
                        returnData += 'Champion kills: ' + summonerData.summary.aggregatedStats.totalChampionKills + '\n'
                        returnData += 'Minion kills: ' + summonerData.summary.aggregatedStats.totalMinionKills + '\n'
                        returnData += 'Assists: ' + summonerData.summary.aggregatedStats.totalAssists + '\n\n'

                        var championName = (summonerData.champions[0].info.name == 'Ahri' ? 'Naruto' : summonerData.champions[0].info.name)
                        returnData += '**Most played Champion data:** \n'
                        returnData += 'Champion: ' + championName + ' - ' + summonerData.champions[0].info.title + '\n'
                        returnData += 'Difficulty: ' + summonerData.champions[0].info.info.difficulty + '\n'
                        returnData += 'Games played: ' + summonerData.champions[0].stats.totalSessionsPlayed + '\n'
                        returnData += 'Champion kills: ' + summonerData.champions[0].stats.totalChampionKills + '\n'
                        returnData += 'Damage dealt: ' + summonerData.champions[0].stats.totalDamageDealt + '\n'
                        returnData += 'Penta kills: ' + summonerData.champions[0].stats.totalPentaKills + '\n'
                        returnData += 'Quadra kills: ' + summonerData.champions[0].stats.totalQuadraKills + '\n'
                        returnData += 'Triple kills: ' + summonerData.champions[0].stats.totalTripleKills + '\n'
                        returnData += 'Double kills: ' + summonerData.champions[0].stats.totalDoubleKills + '\n'
                        returnData += 'Assists: ' + summonerData.champions[0].stats.totalAssists + '\n'
                        returnData += 'Gold earned: ' + summonerData.champions[0].stats.totalGoldEarned + '\n'

                        m.sendMessage(channelID, returnData);
                    });
                });
            });
        });
    });
}

function sortChampions(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a['stats'][property] < b['stats'][property]) ? -1 : (a['stats'][property] > b['stats'][property]) ? 1 : 0;
        return result * sortOrder;
    }
}

module.exports = {
    command: rito,
    sortChampions: sortChampions
}
