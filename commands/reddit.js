//redit
function reddit(user, channelID, parameters, userID)
{
    subUrl = parameters.join('-')
    if(subUrl.charAt(0) === '/')
    {
        subUrl = subUrl.slice(1)
    }
    m.sendMessage(channelID, 'http://reddit.com/' + subUrl)
}

module.exports = {
    command: reddit
}