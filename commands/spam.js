//spam..
function spam(user, channelID, parameters, userID)
{
    amount = parameters.pop()
    string = parameters.join(" ")

    if(amount > 100)
    {
        m.sendMessage(channelID, "Max allowed spam amount is 100.")
        return false
    }

    for(i=0; i < amount; i++)
    {
        m.sendMessage(channelID, string);
    }
}

module.exports = {
    command: spam
}