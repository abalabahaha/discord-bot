var async = require('async')

var getUser = require('./user').getUser

var roleColors = [
    'DEFAULT', 'AQUA', 'GREEN', 'BLUE', 'PURPLE', 'GOLD', 'ORANGE', 'RED', 'GREY', 'DARKER_GREY', 'NAVY', 'DARK_AQUA', 'DARK_GREEN', 'DARK_BLUE', 'DARK_PURPLE', 'DARK_GOLD', 'DARK_ORANGE', 'DARK_RED', 'DARK_GREY', 'LIGHT_GREY', 'DARK_NAVY'
]


function getRole(role_name, server_id, cb)
{
    var serverRole = false

    async.forEachLimit(bot.servers[server_id].roles, 1, function(role, nextRole){
        if(role.name.toLowerCase() == role_name.toLowerCase())
        {
            serverRole = role
        }
        else
        {
            nextRole()
        }
    });

    cb(serverRole)
}

function hasRole(role_name, user_id, channelID, cb)
{
    hasRole = false
    server_id = bot.serverFromChannel(channelID)
    user_roles = bot.servers[server_id].members[user_id].roles

    getRole(role_name, server_id, function(role){
        if(role)
        {
            if(user_roles.indexOf(role.id) >= 0)
            {
                hasRole = true
            }
        }
    });

    if(typeof cb == 'function')
    {
        cb(hasRole)
    }

    return hasRole
}

function createRole(user, channelID, parameters, userID)
{
    role_name = parameters[0]
    role_color = 'DEFAULT'
    serverID = bot.serverFromChannel(channelID)

    if(parameters.length >= 2)
    {
        color = parameters[1].toUpperCase()

        if(color[0] === "#")
        {
            role_color = color
        }
        else
        {
            if(roleColors.indexOf(color) > -1)
            {
                role_color = color
            }
            else
            {
                return m.sendMessage(channelID, "No such color, please use one of these: `" + roleColors.join(', ').toLowerCase() + "`")
            }
        }
        
    }

    function checkRole(res, cb)
    {
        if(bot.servers[serverID].roles[res.id])
        {
            cb(res)
        }
        else
        {
            setTimeout(function(){
                checkRole(res, cb)
            }, 100);
        }
    }

    function editRole(res)
    {
        checkRole(res, function(){
            bot.editRole({
                server: serverID,
                role: res.id,
                name: role_name,
                hoist: false, //or false, will seperate them
                permissions: {
                    GENERAL_CREATE_INSTANT_INVITE: false,
                    TEXT_MENTION_EVERYONE: true,
                    VOICE_CONNECT: true
                }, //Read the permissions readme, IMPORTANT
                color: role_color
            });

            m.sendMessage(channelID, "Role `" + role_name + "` created with color `" + role_color.toLowerCase() + "`.")    
        })
        
    }

    getRole(role_name, serverID, function(exists){
        if(!exists)
        {
            bot.createRole(serverID, function(res) {
                res = JSON.parse(res)
                editRole(res)
            });    
        }
        else
        {
            return m.sendMessage(channelID, "Role with name `" + role_name + "` already exists.")
        }
    });
    
}

function deleteRole(user, channelID, parameters, userID)
{
    var role_name = parameters[0]
    var server_id = bot.serverFromChannel(channelID)

    getRole(role_name, server_id, function(role){
        if(!role)
        {
            return m.sendMessage(channelID, "No role with the name `" + role_name + "` exists in this server.")
        }
        else
        {
            bot.deleteRole({
                server: server_id,
                role: role.id
            }, function(){
                return m.sendMessage(channelID, "Role `" + role_name + "` has been deleted.")
            });
        }
    });
}

function addRole(user, channelID, parameters, userID)
{
    var role_name = parameters[1]
    var server_id = bot.serverFromChannel(channelID)

    var user_id = parameters[0].replace(/<@/, '').replace(/>/, '')

    getRole(role_name, server_id, function(role){
        if(!role)
        {
            return m.sendMessage(channelID, "No role with the name `" + role_name + "` exists in this server.")
        }

        getUser(user_id, function(user){
            if(!user)
            {
                return m.sendMessage(channelID, parameters[0] + " was not found in the database yet, maybe he needs to say something?")
            }
            
            if(hasRole(role_name, user_id, channelID))
            {
                return m.sendMessage(channelID, "User is already added to this role.")   
            }

            bot.addToRole({
                server: server_id,
                user: user.user_id,
                role: role.id
            });
            m.sendMessage(channelID, "Added `" + user.username + "` to the role `" + role.name + "`")
        });
    });

}

function removeRole(user, channelID, parameters, userID)
{
    var role_name = parameters[1]
    var server_id = bot.serverFromChannel(channelID)

    var user_id = parameters[0].replace(/<@/, '').replace(/>/, '')

    getRole(role_name, server_id, function(role){
        if(!role)
        {
            return m.sendMessage(channelID, "No role with the name `" + role_name + "` exists in this server.")
        }

        getUser(user_id, function(user){
            if(!user)
            {
                return m.sendMessage(channelID, parameters[0] + " was not found in the database yet, maybe he needs to say something?")
            }
            
            if(!hasRole(role_name, user_id, channelID))
            {
                return m.sendMessage(channelID, "User does not have the role `" + role_name + "`.")   
            }

            bot.removeFromRole({
                server: server_id,
                user: user.user_id,
                role: role.id
            });
            m.sendMessage(channelID, "Removed role `" + role.name + "` from `" + user.username + "`.")
        });
    });

}


module.exports = {
    roleColors: roleColors,
    getRole: getRole,
    createRole: createRole,
    deleteRole: deleteRole,
    addRole: addRole,
    removeRole: removeRole
}