var request = require('request');
var S = require('string');

//get random number
function random(user, channelID, parameters, userID)
{
    if(isNaN(parseInt(parameters[0])) || isNaN(parseInt(parameters[0])))
    {
        m.sendMessage(channelID, 'Parameters must be numbers.')
        return false;
    }

    var min = parseInt(parameters[0])
    var max = parseInt(parameters[1])
    if(min > max)
    {
        m.sendMessage(channelID, 'Min number can\'t be greater than max number.')
        return false;
    }

    if(min == max)
    {
        m.sendMessage(channelID, 'Min number can\'t be the same as max number.')
        return false;
    }

    request("https://www.random.org/integers/?num=1&min=" + min + "&max=" + max + "&col=1&base=10&format=plain", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            m.sendMessage(channelID, 'Random generated number: ' + S(body).trim().stripTags().s)
        }
    });
}

module.exports = {
    command: random
}