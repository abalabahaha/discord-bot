var S = require('string');

//join server
function join(user, channelID, parameters, userID)
{
    if(S(parameters[0]).startsWith('https://discord.gg/'))
    {
        inviteCode = parameters[0].replace('https://discord.gg/', '')
        bot.acceptInvite(inviteCode, function(res){
            var newServerID = res.guild.id
            m.sendMessage(channelID, 'Joined ' + res.guild.name)
            m.sendMessage(newServerID, 'Hello!\nI am ' + bot.username + ' and I joined your server!\nRun !commands for a list of commands.')
            bot.disconnect();
        });

    }
    else
    {
        m.sendMessage(channelID, 'Invalid invite url')
    }
}

module.exports = {
    command: join
}