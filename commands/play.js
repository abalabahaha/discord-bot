var async = require('async')
var games = require('../db/games')

function play(user, channelID, parameters, userID)
{
    var game = parameters.join(' ');

    var gameID = false;
    var gameName = false;

    async.forEach(games, function(dgame){
        if(isNaN(game))
        {
            if(dgame.name.toLowerCase() == game.toLowerCase())
            {
                gameID = dgame.id
                gameName = dgame.name
            }
        }
        else
        {
            if(dgame.id == game)
            {
                gameID = dgame.id
                gameName = dgame.name
            }   
        }
    });

    if(gameID)
    {
        bot.setPresence({
            game_id: gameID
        })
        m.sendMessage(channelID, "I am now playing `" + gameName + "`.")
    }
    else
    {
        m.sendMessage(channelID, "Game with name/id `" + game + "` not found.")
    }
}

module.exports = {
    play: play
}