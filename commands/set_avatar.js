var request = require('request');
var discordConfig = config.Discord;

function loadBase64Image(url, callback) {
    // Make request to our image url
    request({url: url, encoding: null}, function (err, res, body) {
        if (!err && res.statusCode == 200) {
            // So as encoding set to null then request body became Buffer object
            var base64prefix = 'data:' + res.headers['content-type'] + ';base64,'
                , image = body.toString('base64');
            if (typeof callback == 'function') {
                callback(image, base64prefix);
            }
        } else {
            if (typeof callback == 'function') {
                callback(false, false);
            }
        }
    });
}


function set_avatar(user, channelID, parameters, userID)
{
    var imageUrl = parameters[0];

    loadBase64Image(imageUrl, function(string, prefix){
        if(string === false || prefix === false)
        {
            m.sendMessage(channelID, 'This is not a valid image url, please use a raw image url.');
            return false;
        }

        bot.editUserInfo({
            avatar: string,
            password: discordConfig.password
        }, function(){
            m.sendMessage(channelID, 'Updated my new avatar!\nDo you like it? :blush:.')
        });
    });
}


module.exports = {
    command: set_avatar,
    loadBase64Image: loadBase64Image
}