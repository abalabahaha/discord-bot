var request = require('request');
var S = require('string');

//return a list of anime's
function searchAnime(user, channelID, parameters, userID)
{
    var hummingbirddata = config.HummingBird
    var url = hummingbirddata.url

    var searchString = parameters.join("+").toLowerCase()

    var searchUrl = url + searchString

    request(searchUrl, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var animeData = JSON.parse(body)
            if(animeData.length > 0)
            {
                var results = 'First ' + hummingbirddata.max_entries + ' result(s) for "' + searchString.split('+').join(' ') + '": \n\n'
                var count = 0;
                animeData.slice(0, hummingbirddata.max_entries).forEach(function(entry){
                        results += "Title: " + entry.title + "\n"
                        results += "Community rating: " + parseFloat(entry.community_rating).toFixed(2) + "/5\n"
                        results += "Episodes: " + (entry.episode_count ? entry.episode_count : 'Ongoing') + "\n"
                        results += "Status: " + entry.status + "\n"
                        results += "type: " + entry.show_type + "\n"
                        results += "Period: " + entry.started_airing + " - " + (entry.finished_airing ? entry.finished_airing : 'Ongoing') + "\n"
                        results += "Synopis:\n" + S(entry.synopsis).truncate(250).s + "\n"
                        results += "Url: http://myanimelist.net/anime/" + entry.mal_id + "\n\n"
                })
                m.sendMessage(channelID, results)
            }
            else
            {
                m.sendMessage(channelID, 'No result(s) for "' + searchString.split('+').join(' ') + '".')
            }
        }
        else
        {
            m.sendMessage(channelID, 'No result(s) for "' + searchString.split('+').join(' ') + '".')
            log.error('Failed to run anime data for: ' + searchUrl + '\n' + error)
        }
    })
}

module.exports = {
    command: searchAnime
}