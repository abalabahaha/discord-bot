var async = require('async');

var servers = require('../libs/models').server

var botConfig = config.Bot

function getServer(server_id, cb)
{
    servers.find({where: {server_id: server_id}}).then(function(cserver) {
        if(!cserver)
        {
            cb(false)
        }
        else
        {
            cb(cserver)
        }
    })
}

//send list of commands
function runCommands(user, channelID, parameters, userID)
{
    var commandList = "Command list:\n"

    var usersList = "\nNormal user commands:\n"
    var adminsList = "\nBot Admin commands:\n"

    if(!m.isPrivate(channelID))
    {
        getServer(bot.serverFromChannel(channelID), function(cserver){
            disabled = JSON.parse(cserver.disabled_commands)
            async.forEach(commands, function(scommand){
                if(!scommand.bossOnly && disabled.indexOf(scommand.name) < 0)
                {
                    if(scommand.adminOnly)
                    {
                        adminsList += "- `" + botConfig.command_prefix + scommand.name + "` | " + scommand.description + "\n"
                    }
                    else
                    {
                        usersList += "- `" + botConfig.command_prefix + scommand.name + "` | " + scommand.description + "\n"
                    }
                }
            });

            m.sendMessage(channelID, adminsList)
            m.sendMessage(channelID, usersList)
        });
    }
    else
    {
        async.forEach(commands, function(scommand){
            if(!scommand.bossOnly)
            {
                if(scommand.adminOnly)
                {
                    adminsList += "- `" + botConfig.command_prefix + scommand.name + "` | " + scommand.description + "\n"
                }
                else
                {
                    usersList += "- `" + botConfig.command_prefix + scommand.name + "` | " + scommand.description + "\n"
                }
            } 
        });

        m.sendMessage(channelID, adminsList)
        m.sendMessage(channelID, usersList)
    }

}

function disable_command(user, channelID, parameters, userID)
{
    command_name = parameters[0]

    if(!commands.hasOwnProperty(command_name))
    {
        return m.sendMessage(channelID, "Command `" + command_name + "` doesn't exist.")
    }

    if(command_name == 'disable_command' || command_name == 'enable_command' || command_name == 'disabled_commands')
    {
        return m.sendMessage(channelID, "You can't disable this command.")
    }

    getServer(bot.serverFromChannel(channelID), function(cserver){
        disabled = JSON.parse(cserver.disabled_commands)

        if(disabled.indexOf(command_name) >= 0)
        {
            return m.sendMessage(channelID, "Command `" + command_name + "` is already disabled.")
        }

        disabled.push(command_name)

        cserver.disabled_commands = JSON.stringify(disabled)
        cserver.save().then(function(){
            m.sendMessage(channelID, "Disabled command `" + command_name + "`")
        })
    });
}

function enable_command(user, channelID, parameters, userID)
{
    command_name = parameters[0]

    if(!commands.hasOwnProperty(command_name))
    {
        return m.sendMessage(channelID, "Command `" + command_name + "` doesn't exist.")
    }

    getServer(bot.serverFromChannel(channelID), function(cserver){
        disabled = JSON.parse(cserver.disabled_commands)

        if(disabled.indexOf(command_name) < 0)
        {
            return m.sendMessage(channelID, "Command `" + command_name + "` is not disabled.")
        }

        disabled.splice(disabled.indexOf(command_name), 1)

        cserver.disabled_commands = JSON.stringify(disabled)
        cserver.save().then(function(){
            m.sendMessage(channelID, "Enabled command `" + command_name + "`")
        })
    });
}

function disabled_commands(user, channelID, parameters, userID)
{
    getServer(bot.serverFromChannel(channelID), function(cserver){
        disabled = JSON.parse(cserver.disabled_commands)

        if(disabled.length == 0)
        {
            disabled_string = 'None'
        }
        else
        {
            disabled_string = "`" + disabled.join('`, `') + "`"
        }

        m.sendMessage(channelID, "Disabled commands:\n" + disabled_string)
    });
}

module.exports = {
    command: runCommands,
    disable_command: disable_command,
    enable_command: enable_command,
    disabled_commands: disabled_commands,
    getServer: getServer
}