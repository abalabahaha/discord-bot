//send all to all connected servers
function broadcast(user, channelID, parameters, userID)
{
    Object.keys(bot.servers).forEach(function(server){
        m.sendMessage(server, parameters.join(' '))
    })
}

module.exports = {
    command: broadcast
}