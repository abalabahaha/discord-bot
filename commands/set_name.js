var discordConfig = config.Discord;

function set_name(user, channelID, parameters, userID)
{
    var new_name = parameters.join(' ');

    bot.editUserInfo({
        username: new_name,
        password: discordConfig.password
    }, function(resp){
        m.sendMessage(channelID, 'Updated my new name!\nDo you like it? :blush:.')
    });
}


module.exports = {
    command: set_name
}