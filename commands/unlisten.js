var models = require('../libs/models');

var silent_channel = models.silent_channel;

function unlisten(user, channelID, parameters, userID)
{
    silent_channel.findOne({
        where: {
            channel_id: channelID
        }
    }).then(function(silented){
        if(!silented)
        {
            silent_channel.create({
                channel_id: channelID
            }).then(function(){
                m.sendMessage(channelID, 'I am not listening to this channel anymore.. :(');
            });
        }
    });
}

module.exports = {
    command: unlisten
}