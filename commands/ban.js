var addUserPoints = require('../commands/points').addUserPoints;

//ban user
function ban(user, channelID, parameters, userID)
{
    userid = parameters[0].replace('<@', '').replace('>', '')

    serverid = bot.serverFromChannel(channelID)
    var botserver = bot.servers[serverid]
    var botuser = botserver.members[userid]
    
    if(botuser)
    {
        days = parameters.length >= 2 ? parseInt(parameters[1]) : 1

        if(days > 7)
        {
            return m.sendMessage(channelID, "You can ban a user for a maxmum of 7 days");
        }

        bot.ban({
            channel: botserver.id,
            target: botuser.user.id,
            lastDays : days
        });

        deductPts = days*10
        deductPtsMinus = deductPts*-1

        addUserPoints(bot.id, botuser.user.id, channelID, deductPtsMinus, false, 'ban', function(amount){
            m.sendMessage(channelID, '<@' + botuser.user.id + '> has been banned for ' + days + ' day(s) and lost ' + deductPts + ' point(s) and is now kicked.')
        }); 

    }
    else
    {
        m.sendMessage(channelID, 'No such user found.');
    }

}

module.exports = {
    command: ban
}