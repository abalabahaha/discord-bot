var server = require('../models/server');
var user = require('../models/user');
var server_user = require('../models/server_user');
var command_log = require('../models/command_log');
var chat_log = require('../models/chat_log');
var silent_channel = require('../models/silent_channel');
var points_log = require('../models/points_log');
var points = require('../models/points');
var achievement = require('../models/achievement');
var achievement_user = require('../models/achievement_user');


//server & users
server.belongsToMany(user, { through: server_user, foreignKey: 'server_id' });
user.belongsToMany(server, { through: server_user, foreignKey: 'user_id' });
user.hasMany(command_log, { foreignKey: 'user_id', as: 'Commands'});
user.hasMany(chat_log, { foreignKey: 'user_id', as: 'Messages'});

//chat_log
chat_log.hasOne(user, { foreignKey: 'user_id', as: 'User'});

//command_log
command_log.hasOne(user, { foreignKey: 'user_id', as: 'User'});

//points_log
points_log.hasOne(user, { as: 'user', foreignKey: 'user_id', targetKey: 'user_id'})
points_log.hasOne(user, { as: 'receiver', foreignKey: 'user_id', targetKey: 'receiver_id'})

//points
points.belongsTo(user, { as: 'user', foreignKey: 'user_id', targetKey: 'user_id'})

//achievements
achievement.belongsToMany(user, { through: achievement_user, foreignKey: 'achievement_id' });
achievement.belongsTo(server, { foreignKey: 'server_id' });
user.belongsToMany(achievement, { through: achievement_user, foreignKey: 'user_id' });

module.exports = {
    server: server,
    user: user,
    server_user: server_user,
    command_log: command_log,
    chat_log: chat_log,
    silent_channel: silent_channel,
    points_log: points_log,
    points: points,
    achievement: achievement,
    achievement_user: achievement_user
}