var ini = require('ini')
var fs = require('fs')
var Sequelize = require('sequelize')
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'))

var dbConfig = config.Database;

var sql = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: 'mysql',
  logging: false,

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

  define: {
    timestamps: false // true by default
  },

  timezone: 'Europe/Amsterdam'
});

// export to make available
module.exports = sql