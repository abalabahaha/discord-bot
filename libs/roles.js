var botConfig = config.Bot
var owners = botConfig.owners

function isAdmin(username, userID, channelID) {
    return userID == botConfig.bot_boss ? true : (owners.indexOf(username) >= 0 ? true : (getServerAdmins(bot.serverFromChannel(channelID), userID).indexOf(userID)  >= 0 ? true : false))
}

function getServerAdmins(ServerID, userID)
{
    admins = [];
    admins.push(bot.servers[ServerID].owner_id);

    if(admins.indexOf(botConfig.bot_boss) == -1)
    {
        admins.push(botConfig.bot_boss);
    }

    totalAdmins = admins.concat(usersInRole('hbot admin', ServerID));
    return totalAdmins;
}

function usersInRole(roleName, ServerID)
{
    users = []
    Object.keys(bot.servers[ServerID].roles).forEach(function(key){
        value = bot.servers[ServerID].roles[key];
        if(value.name == roleName)
        {
            roleID = value.id
            Object.keys(bot.servers[ServerID].members).forEach(function(key){
                data = bot.servers[ServerID].members[key]
                data.roles.forEach(function(memberRole){
                    if(roleID == memberRole)
                    {
                        users.push(data.user.id);
                    }
                });
            });
        }
    });

    return users;
}

module.exports = {
    isAdmin: isAdmin,
    inRole: usersInRole,
    getServerAdmins: getServerAdmins
}