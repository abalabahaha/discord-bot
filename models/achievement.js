var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('achievement', {
        description: {
            type: Sequelize.STRING
        },
        points: {
            type: Sequelize.INTEGER
        },
        type: {
            type: Sequelize.ENUM('points', 'messages')
        },
        behavior: {
            type: Sequelize.STRING
        },
        server_id: {
            type: Sequelize.INTEGER,
            field: 'server_id'
        }
}, {
    tableName: 'achievements'
});