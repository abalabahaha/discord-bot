var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('command_log', {
        user_id: {
            type: Sequelize.STRING
        },
        server_id: {
            type: Sequelize.STRING
        },
        channel_id: {
            type: Sequelize.STRING
        },
        command: {
            type: Sequelize.STRING
        },
        parameters: {
            type: Sequelize.STRING
        },
        created: { 
            field: 'created_at',
            type: Sequelize.DATE, 
            defaultValue: Sequelize.NOW
        }
}, {
    tableName: 'command_log'
});