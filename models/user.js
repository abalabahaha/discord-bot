var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('user', {
        username: {
            type: Sequelize.STRING,
        },
        user_id: {
            type: Sequelize.STRING
        },
        coins: {
            type: Sequelize.INTEGER
        }
}, {
    tableName: 'users'
});