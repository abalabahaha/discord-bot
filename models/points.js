var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('points', {
        user_id: {
            type: Sequelize.STRING
        },
        server_id: {
            type: Sequelize.STRING
        },
        amount: { 
            type: Sequelize.INTEGER, 
            defaultValue: 0
        },
        frozen: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
}, {
    tableName: 'points'
});