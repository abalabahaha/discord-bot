var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('server', {
        name: {
            type: Sequelize.STRING,
        },
        server_id: {
            type: Sequelize.STRING
        },
        enable_points: {
            type: Sequelize.BOOLEAN
        },
        disabled_commands: {
            type: Sequelize.TEXT,
            defaultValue: '[]'
        }
}, {
    tableName: 'servers'
});