var Sequelize = require('sequelize')
var sql = require('../libs/db');

module.exports = sql.define('chat_log', {
        user_id: {
            type: Sequelize.STRING
        },
        server_id: {
            type: Sequelize.STRING
        },
        channel_id: {
            type: Sequelize.STRING
        },
        message: {
            type: Sequelize.TEXT
        },
        created: { 
            field: 'created_at',
            type: Sequelize.DATE, 
            defaultValue: Sequelize.NOW
        }
}, {
    tableName: 'chat_log'
});